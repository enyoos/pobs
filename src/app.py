from flask import Flask, render_template, request
from logger import log
from form import RegistrationForm, AuthorizationForm

app = Flask(__name__)
NAME = "pobs"

@app.route("/")
def front_page ( ):
    return render_template( "base.html", name = NAME )

@app.route("/login")
def front_login ( ):
    auth_form = AuthorizationForm(request.form) 
    if request.method == "POST" and form.validate():
        #do something
        log( "post /sign . form : valid " )
    else:
        return render_template( "login_base.html", name = NAME , form = auth_form )

    return render_template( "login_base.html", name = NAME )

@app.route("/sign", methods=['GET', 'POST'] )
def front_sign( ):
    sign_up_form = RegistrationForm(request.form) 
    if request.method == "POST" and form.validate():
        #do something
        log( "post /sign . form : valid " )
    else:
        return render_template( "sign_base.html", name = NAME , form = sign_up_form )

@app.route( "/jobs" )
def front_jobs_pool():
    # make a get request to all the jobs

if __name__ == "__main__":
    log ( " app started listening ")
    app.run()