from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy
# importing the no-sql lib
from w_out_sql import Database, Collection, generate_random_id

uri_sqlite = "sqlite:///databases/clients.db"
uri_no_sql = "./databases/jobs.pst"
app = Flask(__name__)
api = Api(app)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['SQLALCHEMY_DATABASE_URI'] = uri_sqlite
db = SQLAlchemy(app)
nosql_db_clients = Database(path=uri_no_sql)
my_collection_client = Collection(collect_name="clients")
nosql_db_clients.bind_new_collection(my_collection_client)

# class for the normal user
class User(db.Model):
    ### id of the user // make it self increment ?
    id = db.Column(db.Integer, primary_key = True)
    ###name of the user 
    name = db.Column(db.String(50), nullable = False)
    ### password of the user
    password = db.Column( db.String ( 200 ), nullable = False )
    ### email
    email = db.Column( db.String ( 100 ), nullable = False )

    ### image (pp) need to store it in a blob ( binary large obj )
    profile_pic = db.Column( db.LargeBinary(28900) , nullable = True )

    def __repr__(self):
        return f"User : { self.name } | id : { self.id }"


# class for the employer
# how are we going to do it ? check the indeed acc
class Employer(db.Model):
    ### id of the user // make it self increment ?
    id = db.Column(db.Integer, primary_key = True)
    ###name of the user 
    name = db.Column(db.String(50), nullable = False)
    ### password of the user
    password = db.Column( db.String ( 200 ), nullable = False )
    ### email
    email = db.Column( db.String ( 100 ), nullable = False )

    ### image (pp) need to store it in a blob ( binary large obj ) premits a 170 x 170 image
    profile_pic = db.Column( db.LargeBinary(28900) , nullable = True )

    ### the compagny name 
    comp_name = db.Column(db.String ( 100 ), nullable = False )

    def __repr__( self ):
        return f"Emloyee : { self.name } | id : { self.id }"

###for the PUT HTTP method \ for the post method
user_put_args = reqparse.RequestParser()
user_put_args.add_argument(
    "name", type = str, help = "Name of the user is required", required = True
)
user_put_args.add_argument(
    "password", type = str, help = "the password of the user is required", required = True
)
user_put_args.add_argument(
    "email", type = str, help = "email of the user is required", required = True
)

###for the update HTTP method
user_update_args = reqparse.RequestParser()
user_update_args.add_argument(
    "name", type = str, help = "@ name of the user", required = False
)

user_update_args.add_argument(
    "password", type = str, help = "@ pass of the user ", required = False
)

user_update_args.add_argument(
    "email", type = str, help = "@ email of the user ", required = False
)

resource_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'email': fields.String, 
    'password': fields.String
}


# http request class ( handling the http requests \ user )
class UserEndPoint(Resource):

    @marshal_with(resource_fields)
    # querying by the name ( string )
    def get(self, name : str ):
        result = AtomeModel.query.filter_by(name = name).first()

        if not result:
            abort(404, message = "user not found") 
        
        return result
    

    @marshal_with(resource_fields)
    def put(self, name):
        args = user_put_args.parse_args() # parsing the body of the thing
        result = AtomeModel.query.filter_by(name = name).first()
        
        if result:
            abort(409, message = "user with that name already in the database")
        
        ###makes sense now
        atome = AtomeModel(
            # id = num_protons,  this thing is autoincrementing 
            name = args['name'],
            email = args['email'],
            password = args['password'],
        )

        db.session.add(atome)
        db.session.commit()

        return result


    @marshal_with(resource_fields)
    def patch(self, name ):
        args = user_update_args.parse_args()
        result = AtomeModel.query.filter_by(name = name).first()

        if not result:
            abort(404, message = "the user doesnt exist in the db")
        
        else:
            if args['name']:
                result.name = args['name'] 
            if args['email']:
                result.email = args['email'] 
            if args['password']:
                result.password = args['password']

            db.session.commit()
        return result


    def delete(self, name ):
        result = AtomeModel.query.filter_by(name = name ).first()

        if not result:
            abort(404, message = "the user not found")
 
        db.session.delete(result) #better than "result.delete()"
        db.session.commit()

        return result, "DELETED"

# http request class ( handling the http requests \ employer )
class EmployerEndPoint(Resource):

    @marshal_with(resource_fields)
    # querying by the name ( string )
    def get(self, name : str ):
        result = AtomeModel.query.filter_by(name = name).first()

        if not result:
            abort(404, message = "user not found") 
        
        return result
    

    @marshal_with(resource_fields)
    def put(self, name):
        args = user_put_args.parse_args() # parsing the body of the thing
        result = AtomeModel.query.filter_by(name = name).first()
        
        if result:
            abort(409, message = "user with that name already in the database")
        
        ###makes sense now
        atome = AtomeModel(
            # id = num_protons,  this thing is autoincrementing 
            name = args['name'],
            email = args['email'],
            password = args['password'],
        )

        db.session.add(atome)
        db.session.commit()

        return result


    @marshal_with(resource_fields)
    def patch(self, name ):
        args = user_update_args.parse_args()
        result = AtomeModel.query.filter_by(name = name).first()

        if not result:
            abort(404, message = "the user doesnt exist in the db")
        
        else:
            if args['name']:
                result.name = args['name'] 
            if args['email']:
                result.email = args['email'] 
            if args['password']:
                result.password = args['password']

            db.session.commit()
        return result


    def delete(self, name ):
        result = AtomeModel.query.filter_by(name = name ).first()

        if not result:
            abort(404, message = "the user not found")
 
        db.session.delete(result) #better than "result.delete()"
        db.session.commit()

        return result, "DELETED"


# creating the jobs pool
class Jobs:
    def __init__( self, name_employer : str , jb_title : str, id : str, requirements : list , loc : str , date_post : str ): #  wait until we make the form for employement
        self.name_employer = name_employer
        self.jb_title = jb_title
        self.id = id
        self.requirements = requirements
        self.loc = loc
        self.date_post = date_post

    def compare_two_jobs( job_loc1, job_loc2 ) -> int :
        print ( "not implemented ") # using the google map api ? 

    def to_dict(self):
        return {
            "name_employer" : self.name_employer,
            "jb_title" : self.jb_title,
            "id" : self.id,
            "requirements" : self.requirements,
            "loc" : self.loc,
            "date_post": self.date_post
        }

    def __repr__(self):
        return "[JOB : {id}] title : {title}, employer : {name_employer}, requirements : {requirements}, location: {loc}, date : {date_post}".format(id = id,
                                title = jb_title, name_employer=name_employer, requirements = requirements, loc=loc, date_post=date_post)


api.add_resource(UserEndPoint, "/users/<string:name>")
api.add_resource(EmployerEndPoint , "/employers/<string:name>")


# remember to use the var for the uri ( no sql one )
# testing : check the test_routes.py file
@app.route ( "/jobs/<string:job_title>", methods=['GET', 'POST', 'DELETE', 'PUT'] )
def routing_string_jobs_no_sql(job_title : str ):
    if request.method == "POST":
        args = request.json
        try : 
            # checking if all the required thing are in the post data
            # no need if statement since is except an attribute error 
            # the id will be generated
            persist_obj_jobs = Jobs (
                name_employer=args.name_employer,
                jb_title = job_title,
                id=generate_random_id(),
                requirements=args.requirements,
                loc=args.loc,
                date_post=args.date_post
            )
            my_collection_client.add_record(persist_obj_jobs.to_dict())
            # remember to save
            nosql_db_clients.save()
            return "good"
        except AttributeError as err:
            return "{err} [HINT : missing data for the post ]".format(err= err)

    elif request.method == "GET":
        if not job_title == "pool":
            print ( "received a GET request from the jobs endpoint ", job_title)
            return 'made a get request' 
        
        print ( "received a get request from the job endpoint ", "pool")
        return "made a pool request"
    
    elif request.method == "DELETE":
        print ( "deleting this : ", job_title)
        return 'made a delete request' 

    elif request.method == "PUT":
        args = request.json
        print ( "args received for the updte : ", args)
        return 'made a put request'
       
    return "your thing is wierd"

if __name__ == "__main__":
    app.run(debug = True)