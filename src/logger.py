import logging

logger = logging.getLogger( 'my app' )
logger.setLevel( logging.DEBUG )
fh = logging.FileHandler( './logs/info.log' )
fh.setLevel( logging.DEBUG )
logger.addHandler( fh )

def log( text ): logger.info(text)